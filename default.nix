{ baseNixpkgs ? (import <nixpkgs> {}) }:

let
  nixpkgsSrc = baseNixpkgs.callPackage ./nixpkgs.nix {};
  nixpkgs = import nixpkgsSrc {};
in with nixpkgs;

let
  fetchGhc =
    { version, sha256
    , url ? null
    }: fetchTarball {
      url = if url != null then url else "https://downloads.haskell.org/ghc/${version}/ghc-${version}-src.tar.xz";
      inherit sha256;
    };

  fetchGhcGit = { ref, sha256 }: fetchgit {
      url = "https://gitlab.haskell.org/ghc/ghc";
      rev = ref;
      inherit sha256;
      fetchSubmodules = true;
  };

  buildGhc = {
    version, url ? null, sha256 ? null,
    src ? (fetchGhc { inherit url version sha256; }),
    bootVer, werror ? false, sphinx ? python3Packages.sphinx,
    happy ? haskellPackages.happy,
    alex ? haskellPackages.alex
  }:
    callPackage ./ghc.nix {
      inherit version src;
      bootPkgs = haskell.packages."${bootVer}";
      buildLlvmPackages = llvmPackages;
      inherit werror sphinx happy alex;
    };

  happy-1_20_0 =
    let src = fetchTarball {
      url = "http://hackage.haskell.org/package/happy-1.20.0/happy-1.20.0.tar.gz";
      sha256 = "sha256:095qp013kkppgy2vw0vi3hkahbdif3mas3ryb06psn2vbqd5b47l";
    };
    in haskellPackages.callCabal2nix "happy" src {};

  alex-3_2_6 =
    let src = fetchTarball {
      url = "http://hackage.haskell.org/package/alex-3.2.6/alex-3.2.6.tar.gz";
      sha256 = "sha256:17vnxwbzsp82mg3174c4pyp68ghfbr0dyrw61cmjjqdx32l93r77";
    };
    in haskellPackages.callCabal2nix "alex" src {};

  masterSrc = fetchGhcGit {
    ref = "d44e42a26e54857cc6174f2bb7dc86cc41fcd249";
    sha256 = "sha256:0v59qr3yir19x74m6s38jcavc3n3z5rk2inkij9npprhqvc86v2m";
  };
in
{
  #
  # GHC 8.2
  #
  "ghc-8_2_1" = buildGhc {
    version = "8.2.1";
    sha256 = "sha256:12lwazpfxiv7c6rjiz54w8wrc9fdjziiy9vvdm9g31wfdgazmaih";
    bootVer = "ghc822Binary";
    sphinx = null; # Sphinx incompatible
  };

  #
  # GHC 8.4
  #
  "ghc-8_4_1" = buildGhc {
    version = "8.4.1";
    sha256 = "sha256:1i8p2l654pm1qsjg8z01mm29rhyqqzx2klm3daqwspql5b3chnna";
    bootVer = "ghc822Binary";
    sphinx = null; # Sphinx incompatible
  };
  "ghc-8_4_2" = buildGhc {
    version = "8.4.2";
    sha256 = "sha256:0d19cq7rmrbnv0dabxgcf9gadjas3f02wvighdfgr6zqr1z5fcrc";
    bootVer = "ghc822Binary";
    sphinx = null; # Sphinx incompatible
  };
  "ghc-8_4_3" = buildGhc {
    version = "8.4.3";
    sha256 = "sha256:1y8bd6qxi5azqwyr930val428r2yi9igfprv11acd02g7d766yxq";
    bootVer = "ghc822Binary";
    sphinx = null; # Sphinx incompatible
  };

  #
  # GHC 8.6
  #
  "ghc-8_6_2" = buildGhc {
    version = "8.6.2";
    sha256 = "sha256:1spb0jfxv3r0f12z6ys1y1ssnqwnnqavr947gnz7a74y6k8kb2h2";
    bootVer = "ghc865";
  };
  "ghc-8_6_3" = buildGhc {
    version = "8.6.4";
    sha256 = "sha256:1iz7lsw12vkl6da7cqbxiy5hidkfp3427fm30qv3294pvx2c2szr";
    bootVer = "ghc865";
  };
  "ghc-8_6_4" = buildGhc {
    version = "8.6.4";
    sha256 = "sha256:1iz7lsw12vkl6da7cqbxiy5hidkfp3427fm30qv3294pvx2c2szr";
    bootVer = "ghc865";
  };
  "ghc-8_6_5" = buildGhc {
    version = "8.6.5";
    sha256 = "sha256:0p7ykswxid024aqq0aqd91yla719kc1rnb5f90ply43xk9457687";
    bootVer = "ghc865";
  };

  #
  # GHC 8.8
  #
  "ghc-8_8_1" = buildGhc {
    version = "8.8.1";
    sha256 = "sha256:07ag4ah5dd65l6kxrrx1k9zxw0fswy2ias5kqs61rxif00a982jb";
    bootVer = "ghc865";
  };
  "ghc-8_8_2" = buildGhc {
    version = "8.8.2";
    sha256 = "sha256:1qf4nrrxn0fnfvxri5m34nx6f3x2px85xhxnypapa4j3y3dh5xvj";
    bootVer = "ghc865";
  };
  "ghc-8_8_3" = buildGhc {
    version = "8.8.3";
    sha256 = "sha256:1xmdrr6cj4i14ginfnprkrj5fh6d621r70fb93m8a72hk6ca9alg";
    bootVer = "ghc865";
  };
  "ghc-8_8_4" = buildGhc {
    version = "8.8.4";
    sha256 = "sha256:1vbgjxpl7s4nyfzmdzxy7zcpdzlhnn1zalb497p2w7bf0qamzmdq";
    bootVer = "ghc865";
  };

  #
  # GHC 8.10
  #
  "ghc-8_10_1" = buildGhc {
    version = "8.10.1";
    sha256 = "sha256:1jhs396lww687121mdvb723n8ql1rv60x9s3xxy7c9h749gy1n75";
    bootVer = "ghc884";
  };
  "ghc-8_10_2" = buildGhc {
    version = "8.10.2";
    sha256 = "sha256:03g5k48cm8758fz4y3yv14r5p46cxcspjwy5flr6b1s49ciy2s79";
    bootVer = "ghc884";
  };
  "ghc-8_10_3" = buildGhc {
    version = "8.10.3";
    sha256 = "sha256:0phgyrmmnni6cpzah7pgx9plql84bc24c4yhrjvcw2ksk3lncx0g";
    bootVer = "ghc884";
  };
  "ghc-8_10_4" = buildGhc {
    version = "8.10.4";
    sha256 = "sha256:1blpmp67i0lb1pcmwg8lpf6cg3mpim3adzg8fjmrvil8yr4wp1w9";
    bootVer = "ghc884";
  };

  #
  # GHC 9.0
  #
  "ghc-9_0_1" = buildGhc {
    version = "9.0.1";
    url = "https://downloads.haskell.org/ghc/9.0.1/ghc-9.0.1-src.tar.xz";
    sha256 = "sha256:1wp240i99gccs21agjq0249lv6raffblzqkl4vpnlj290dycwb9h";
    bootVer = "ghc8101";
  };

  #
  # GHC 9.2
  #
  "ghc-9_2_1-alpha1" = buildGhc {
    version = "9.2.1-alpha1";
    url = "https://downloads.haskell.org/ghc/9.2.1-alpha1/ghc-9.2.0.20210331-src.tar.xz";
    sha256 = "sha256:0ska5649fn672w97nwh6xvx4yc1dbik751mxdqgmxl9sq8wk1h18";
    bootVer = "ghc8101";
  };

  #
  # master
  #
  "ghc-master" = buildGhc {
    version = "9.1.0";
    src = masterSrc;
    sphinx = python3Packages.sphinx;
    bootVer = "ghc8101";
    happy = happy-1_20_0;
    alex = alex-3_2_6;
  };

  #
  # GHC 9.0 (experimental Hadrian derivation)
  #
  "hadrian-ghc_9_0_1" = callPackage ./hadrian.nix {
    version = "9.0.1";
    src = fetchGhc {
      version = "9.0.1";
      url = "https://downloads.haskell.org/ghc/9.0.1/ghc-9.0.1-src.tar.xz";
      sha256 = "sha256:1wp240i99gccs21agjq0249lv6raffblzqkl4vpnlj290dycwb9h";
    };
    sphinx = python3Packages.sphinx;
    buildLlvmPackages = llvmPackages;
    bootGhc = "ghc8101";
    libffi = null; # FIXME: hadrian's with-system-libffi support is broken
  };

  cabal-install = callPackage ./cabal-install.nix {};
  nixpkgs = nixpkgs;
}
